# 4-国五大人

#### 介绍
TOPIC_ID:4, TEAM_ID:1491717093, TEAM_NAME:国五大人.

#### 软件架构
软件架构说明
该任务调度框架分为三个模块：初始化程序、控制程序、任务调度程序。
初始化程序负责启动控制程序和任务调度程序，并在启动过程中传递必要的参数（进程pid等），之后自动结束运行。
控制程序负责接收命令输入，处理后将相应信号发送给任务调度程序，控制程序也会按照系统时钟频率向任务调度程序发送时钟信号。
在测试模式下，使用替代初始化程序的测试程序来进行测试。

#### 安装教程

1.  在Ubuntu操作系统中，下载项目源代码至任意目录下（推荐使用Ubuntu20.04版本的系统）。
2.  进入 "Schedule/lib" 目录，以root权限执行"make"命令编译动态库文件（将编译好的动态库文件移动到 "/usr/lib" 目录下需要root权限）。
3.  返回项目主目录下，以普通用户权限执行"make"命令进行编译，得到init、test、control、LUTF四个可执行文件。
4.	如果想要在修改动态库代码后更新动态库文件，只需重新执行步骤2。

#### 使用说明

正常使用：
1.  编译结束后，在项目主目录下输入"./init"命令即可运行任务调度框架。
2.  任务调度框架执行后会打开两个新的bash，分别执行控制程序和任务调度程序。
3.  用户可以在运行控制程序的bash中输入命令（仅支持"man"显示帮助手册、"task -c"创建新任务命令、"exit"退出任务调度框架命令），并在任务调度程序的bash中查看任务运行结果。

测试模式
1.  编译结束后，在项目主目录下输入"./test"命令即可进入测试模式。
2.  测试模式下，控制程序会自动给任务调度程序发送1000000个任务相关命令。(相关bug已修复)
3.  用户可以在任务调度程序的bash中查看任务运行结果。


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
