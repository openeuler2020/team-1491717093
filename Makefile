all : control init test LUTF
.PHONY : all
CC = gcc
INC = -I ./Control -I ./Init -I ./Schedule
OBJ = obj
OBJF = libdynamic.so
SUBDIR = -I ./Schedule/lib
MAKE = make
LDFLAGS = -L./Schedule/lib/libs
LIBS = -ldynamic
INCLUDE := -I./Schedule/lib/dynamic.h
$(OBJ):
	mkdir obj

control : $(OBJ)/control.o
	$(CC) -o control $(OBJ)/control.o

init : $(OBJ)/init.o
	$(CC) -o init $(OBJ)/init.o $(LDFLAGS) $(LIBS)

test : $(OBJ)/test.o
	$(CC) -o test $(OBJ)/test.o $(LDFLAGS) $(LIBS)

LUTF : $(OBJ)/LUTF.o $(OBJ)/signal_handle.o
	$(CC) -o LUTF $(OBJ)/LUTF.o $(OBJ)/signal_handle.o $(LDFLAGS) $(LIBS)

$(OBJ)/control.o: Control/control.c
	$(CC) -c Control/control.c -o $(OBJ)/control.o $(INC)

$(OBJ)/init.o: Init/global_init.c
	$(CC) -c Init/global_init.c -o $(OBJ)/init.o $(INC)

$(OBJ)/test.o: Init/global_test.c
	$(CC) -c Init/global_test.c -o $(OBJ)/test.o $(INC)

$(OBJ)/signal_handle.o: Schedule/signal_handle.c
	$(CC) -c Schedule/signal_handle.c -o $(OBJ)/signal_handle.o $(INC)

$(OBJ)/LUTF.o: Schedule/LUTF.c
	$(CC) -c Schedule/LUTF.c -o $(OBJ)/LUTF.o $(INC)

subsystem:
	$(MAKE) -C $(SUBDIR)

.PHONY: clean
clean:
	rm -f *.o
