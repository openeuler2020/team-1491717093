/**************************************************************************
*	赛题4：LUTF - Linux Userspace Task Framework
*	团队：国五大人  1491717093
*
*
*	文件名：signal_handle.c
*	简介：该文件中定义了所有的信号处理函数，接收来自控制进程（测试进程）
* 的信号，并做出对应的处理（创建任务、删除任务、调整任务优先级、重新初始化任务队列等）
*
**************************************************************************/

extern struct LUTF_task_struct* curr_task;//当前任务指针，指向当前执行任务的LUTF_task_struct结构体
extern unsigned int task_queue_valid;//表示32个任务队列
extern struct LUTF_task_queue* global_task_queue;//指向任务队列的全局指针
extern int handling_task;//表示是否正在处理任务函数中
extern unsigned long global_task_pid;//全局任务pid
extern long int global_clock;//全局时钟，相当于jiffies

#include "LUTF.h"

extern jmp_buf main_env;//函数间跳转时保存上下文的环境变量

void signal_clock_interrupt(int sign )//时钟中断
{
	global_clock += 1;//全局时钟加一

	if (handling_task == 1)//只有在处理任务时需要保存上下文并跳转回主函数
	{
		if (sign == CLOCK_INTERRUPT) {
			//设置表示保存上下文的标识
			curr_task->if_jump = 1;
			// set up jumping point
			if (!setjmp(curr_task->jump_addr))
			{
				//跳转回主函数
				handling_task = 0;//退出任务函数处理
				longjmp(main_env, 1);
					
			}
		}
	}
}

void signal_task_create(int sign)//创建任务，并加入任务队列
{
	struct LUTF_task_struct* task_struct = (struct LUTF_task_struct*)malloc(sizeof(struct LUTF_task_struct));
	
	task_struct->pid = global_task_pid++;
	if(global_task_pid%5478==1)
		srand((unsigned) time(NULL));
	task_struct->type = ((int)rand()) % 20+1;
	task_struct->priority = global_task_pid % 32;
	task_struct->if_jump = 0;
	//task_struct->jump_addr = NULL;

	if ((task_queue_valid & (0x1 << (31 - task_struct->priority))) != (0x1 << (31 - task_struct->priority)))//原本该优先级队列为空
	{
		task_queue_valid = task_queue_valid | (0x1 << (31 - task_struct->priority));//设置优先级队列的有效性
		task_struct->next_task = task_struct;
		task_struct->pre_task = task_struct;
		global_task_queue[task_struct->priority].task_num = 1;
		global_task_queue[task_struct->priority].task_struct_start = task_struct;
	}
	else//原本该优先级队列不为空
	{
		
		task_struct->next_task = global_task_queue[task_struct->priority].task_struct_start;
		task_struct->pre_task = global_task_queue[task_struct->priority].task_struct_start->pre_task;
		global_task_queue[task_struct->priority].task_struct_start->pre_task->next_task = task_struct;
		global_task_queue[task_struct->priority].task_struct_start->pre_task = task_struct;
		global_task_queue[task_struct->priority].task_num += 1;

	}
	//printf("signal:%d\n", task_queue_valid);
}
/*
void signal_task_delete(int sign)//删除任务队列中的一个任务
{
	;
}

void signal_task_prior(int sign)//修改任务队列中的优先级
{
	;
}

void signal_frame_init(int sign)//初始化任务框架
{
	;
}

void signal_frame_load(int sign)//动态显示任务框架的负载（类似top命令）
{
	;
}
*/
void signal_exit_normal(int sign)//停止接受新任务，在所有任务处理完毕后结束任务调度程序
{
	printf("LUTF exits normally. You can check the result or close this terminal.\n");
	exit(0);
}

/*void signal_exit_immediate(int sign)//立刻结束任务调度程序
{
	exit(0);
}*/
