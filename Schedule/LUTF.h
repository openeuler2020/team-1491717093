/**************************************************************************
*	赛题4：LUTF - Linux Userspace Task Framework
*	团队：国五大人  1491717093
*
*	文件名：LUTF.h
*	简介：该头文件中包含对任务中其他头文件的引用，以及一些函数声明及宏定义
*
**************************************************************************/

#ifndef _LUTF_H_
#define _LUTF_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <signal.h>
#include <setjmp.h>
#include <time.h>
#include "dynamic.h"
#include "control.h"


//信号处理函数
void signal_clock_interrupt(int sign);//时钟中断
void signal_task_create(int sign);//创建任务，并加入任务队列
void signal_task_delete(int sign);//删除任务队列中的一个任务
void signal_task_prior(int sign);//修改任务队列中的优先级
void signal_frame_init(int sign);//初始化任务框架
void signal_frame_load(int sign);//动态显示任务框架的负载（类似top命令）
void signal_exit_normal(int sign);//停止接受新任务，在所有任务处理完毕后结束任务调度程序
void signal_exit_immediate(int sign);//立刻结束任务调度程序

int init_task_queue(struct LUTF_task_queue* task_queue);//任务队列初始化
int start_task(struct LUTF_task_struct* task_struct, int i);//开始执行任务


#define CAPIBILITY 1000 //性能上限灰度，可以控制整个框架最多支持的任务数
#define MAXTASK 1024 //基础任务数量，与性能上限灰度相乘后即为整个框架最多支持的任务数
#define TIMER 0 //是否引入时间片的抢占方法  //进程在使用完时间片后，在队列中后移 i*a^(1/2)个位置
#define SCHEDULE 1//任务调度方法，1代表静态优先级FIFO调度
#define CLOCK_INTERRUPT 1//该中断设定的宏


struct LUTF_task_struct//表示任务
{
	unsigned long int pid; //任务唯一标识符
	int type;//任务种类，对应共享库中的函数编号
	int priority;//任务优先级，可由输入指定，不指定时根据任务种类设定默认值
	struct LUTF_task_struct* next_task;//指向后一个任务
	struct LUTF_task_struct* pre_task;//指向前一个任务

    //represents if jumping context exits
    //Attention: if_jump is supposed to recover back to 0 after complete excution
    int if_jump;
    //point to jumping context addr
    //Attention: free the malloced space after complete excution
    jmp_buf jump_addr;

#if TIMER
	int timer;//剩余时间片，在不设置TIMER机制时为无效参量
	int times;//第几次轮询到该任务
#endif
};


struct LUTF_task_queue//表示任务队列
{
	int task_num;//任务队列中的任务数目
	struct LUTF_task_struct *task_struct_start;//任务实例双向链表头
};


// // This struct is used to store the function context
//PY decides to abondon using this struct
// struct task_context_store{
//     //function context
//     jmp_buf env;
//     //以下为dfm所定义task结构体
//     struct LUTF_task_struct info;
// }Task_Record[MAX_TASK];


//以下变量用于记录有多少个上下文被保存
// This variable is temporily planned not to use 
// int Record_Task = 0;


#endif

