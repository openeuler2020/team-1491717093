/**************************************************************************
*	赛题4：LUTF - Linux Userspace Task Framework
*	团队：国五大人  1491717093
*
*	文件名：dynamic.c
*	简介：该文件中包含了动态库相关的函数定义，
* 任务调度框架中所有的可用任务都定义在该文件中
*
**************************************************************************/

#include"dynamic.h"

#define LIMIT 2147483648//用于整数运算时的数值溢出检查

int LUTF_add_i(int a, int b)//1.整数加运算 任务编号1
{
	if (((long int)a + (long int)b <= LIMIT - 1) && ((long int)a + (long int)b >= -1 * LIMIT))
	{
		printf("(int)%d+%d=%d\n", a, b, a + b);
		return 0;
	}
	else
	{
		printf("Error! Data overflow\n");
		return -1;
	}
}

int LUTF_sub_i(int a, int b)//2.整数减运算 任务编号2
{
	if (((long int)a - (long int)b <= LIMIT - 1) && ((long int)a - (long int)b >= -1 * LIMIT))
	{
		printf("(int)%d-%d=%d\n", a, b, a - b);
		return 0;
	}
	else
	{
		printf("Error! Data overflow\n");
		return -1;
	}
}

int LUTF_mul_i(int a, int b)//3.整数乘运算 任务编号3
{
	if (((long int)a * (long int)b <= LIMIT - 1) && ((long int)a * (long int)b >= -1 * LIMIT))
	{
		printf("(int)%dx%d=%d\n", a, b, a * b);
		return 0;
	}
	else
	{
		printf("Error! Data overflow\n");
		return -1;
	}
}

int LUTF_div_i(int a, int b)//4.整数除运算 任务编号4
{
	if (b != 0 && ((long int)a/(long int)b<=LIMIT-1)&& ((long int)a / (long int)b >= -1*LIMIT))
	{
		printf("(int)%d/%d=%d\n", a, b, a / b);
		return 0;
	}
	else if(b==0)
	{
		printf("Error! The divisor is 0\n");
		return -1;
	}
	else
	{
		printf("Error! Data overflow\n");
		return -1;
	}
}

double LUTF_add_d(double a, double b)//5.浮点数加运算 任务编号5
{
	printf("%lf+%lf=%lf\n", a, b, a+b);
	return 0;
}

double LUTF_sub_d(double a, double b)//6.浮点数减运算 任务编号6
{
	printf("%lf-%lf=%lf\n", a, b, a-b);
	return 0;
}

double LUTF_mul_d(double a, double b)//7.浮点数乘运算 任务编号7
{
	printf("%lfx%lf=%lf\n", a, b, a*b);
	return 0;
}

double LUTF_div_d(double a, double b)//8.浮点数除运算 任务编号8
{
	printf("%lf/%lf=%lf\n", a, b, a/b);
	return 0;
}
//sin 
double LUTF_sin_d(double a){
	printf("sin(%lf)=%lf\n", a, sin(a));
	return 0;
}
//cos
double LUTF_cos_d(double a){
	printf("cos(%lf)=%lf\n", a, cos(a));
	return 0;
}
//tan 
double LUTF_tan_d(double a){
	printf("tan(%lf)=%lf\n", a, tan(a));
	return 0;
}
//cot
double LUTF_cot_d(double a){
	printf("cot(%lf)=%lf\n", a, 1/tan(a));
	return 0;
}
double LUTF_pow_d(double a, double b){
	printf("pow(%lf,%lf)=%lf\n", a, b, pow(a, b));
	return 0;
}
double LUTF_sqrt_d(double a){
	printf("sqrt(%lf)=%lf\n", a, sqrt(a));
	return sqrt(a);
}

//there are 6 remains
double LUTF_fabs_d(double a){
	printf("fabs(%lf)=%lf\n", a, fabs(a));
	return fabs(a);
}

double LUTF_exp_d(double a){
	printf("exp(%lf)=%lf\n", a, exp(a));
	return exp(a);
}

double LUTF_log_d(double a){
	printf("log(%lf)=%lf\n", a, log(a));
	return log(a);
}

double LUTF_log10_d(double a){
	printf("log10(%lf)=%lf\n", a, log10(a));
	return log10(a);
}

//取上整
double LUTF_ceil_d(double a){
	printf("ceil(%lf)=%lf\n", a, ceil(a));
	return ceil(a);
}

//取下整
double LUTF_floor_d(double a){
	printf("floor(%lf)=%lf\n", a, floor(a));
	return floor(a);
}
