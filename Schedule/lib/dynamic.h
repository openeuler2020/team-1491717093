/**************************************************************************
*	赛题4：LUTF - Linux Userspace Task Framework
*	团队：国五大人  1491717093
*	
*	文件名：dynamic.h
*	简介：该头文件中包含动态库相关的头文件引用，以及一些函数声明及宏定义
*
**************************************************************************/

#ifndef _DYNAMIC_H_
#define _DYNAMIC_H_

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include <math.h>

#define TASK_NUM 8//代表一共有多少种任务函数


int LUTF_add_i(int a, int b);//1.整数加运算 任务编号1
int LUTF_sub_i(int a, int b);//2.整数减运算 任务编号2
int LUTF_mul_i(int a, int b);//3.整数乘运算 任务编号3
int LUTF_div_i(int a, int b);//4.整数除运算 任务编号4
double LUTF_add_d(double a, double b);//5.浮点数加运算 任务编号5
double LUTF_sub_d(double a, double b);//6.浮点数减运算 任务编号6
double LUTF_mul_d(double a, double b);//7.浮点数乘运算 任务编号7
double LUTF_div_d(double a, double b);//8.浮点数除运算 任务编号8
double LUTF_sin_d(double a);//9.sin运算 任务编号9
double LUTF_cos_d(double a);//10.cos运算 任务编号10
double LUTF_tan_d(double a);//11.tan运算 任务编号11
double LUTF_cot_d(double a);//12.cot运算 任务编号12
double LUTF_pow_d(double a,double b);//13.乘方运算 任务编号13
double LUTF_sqrt_d(double a);//14.开方运算 任务编号14
double LUTF_fabs_d(double a);//15.以e为底的log运算 任务编号15
double LUTF_exp_d(double a);//16.以e为底的乘方运算 任务编号16
double LUTF_log_d(double a);//17.以e为底的log运算 任务编号17
double LUTF_log10_d(double a);//18.以10为底的log运算 任务编号18
double LUTF_ceil_d(double a);//19.取上整运算 任务编号19
double LUTF_floor_d(double a);//20.取下整运算 任务编号20

//将动态库函数调用同一格式
int (*task_type_1)(int, int) = LUTF_add_i;
int (*task_type_2)(int, int) = LUTF_sub_i;
int (*task_type_3)(int, int) = LUTF_mul_i;
int (*task_type_4)(int, int) = LUTF_div_i;
double (*task_type_5)(double, double) = LUTF_add_d;
double (*task_type_6)(double, double) = LUTF_sub_d;
double (*task_type_7)(double, double) = LUTF_mul_d;
double (*task_type_8)(double, double) = LUTF_div_d;
double (*task_type_9)(double)  = LUTF_sin_d;
double (*task_type_10)(double) = LUTF_cos_d;
double (*task_type_11)(double) = LUTF_tan_d;
double (*task_type_12)(double) = LUTF_cot_d;
double (*task_type_13)(double,double) = LUTF_pow_d;
double (*task_type_14)(double) = LUTF_sqrt_d;
double (*task_type_15)(double) = LUTF_fabs_d;
double (*task_type_16)(double) = LUTF_exp_d;
double (*task_type_17)(double) = LUTF_log_d;
double (*task_type_18)(double) = LUTF_log10_d;
double (*task_type_19)(double) = LUTF_ceil_d;
double (*task_type_20)(double) = LUTF_floor_d;

#endif
