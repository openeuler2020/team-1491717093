/**************************************************************************
*	赛题4：LUTF - Linux Userspace Task Framework
*	团队：国五大人  1491717093
*	
*	文件名：control.h
*	简介：该头文件中包含控制程序相关的头文件引用，以及一些函数声明及宏定义
*
**************************************************************************/
#ifndef _CONTROL_H_
#define _CONTROL_H_
#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<stdlib.h>
#include<ctype.h>
#include<signal.h>

#define TRUE 1
#define FALSE 0
#define BUF_SIZE 256 //输入字符串的上限

#define Hz 100 //时钟频率
#define SIG_TIMER __SIGRTMIN + 2 //中断信号

#define SIG_ERROR -1
#define SIG_TASK_CREATE __SIGRTMIN + 10 //创建任务，并加入任务队列
#define SIG_TASK_DELETE __SIGRTMIN + 11 //删除任务队列中的一个任务
#define SIG_TASK_PRIOR __SIGRTMIN + 12 //修改任务队列中的优先级
#define SIG_FRAME_INIT __SIGRTMIN + 13 //初始化任务框架
#define SIG_FRAME_LOAD __SIGRTMIN + 14 //动态显示任务框架的负载（类似top命令）
#define SIG_EXIT_NORMAL __SIGRTMIN + 15 //停止接受新任务，在所有任务处理完毕后结束任务调度程序
#define SIG_EXIT_IMMEDIATE __SIGRTMIN + 16 //立刻结束任务调度程序

static int global_pid;

static const char* COMMAND_MAN = "man"; //帮助指令
static const char* COMMAND_TASK = "task"; //任务相关
static const char* COMMAND_TASK_CREATE = "-c"; //创建任务，并加入任务队列
static const char* COMMAND_TASK_DELETE = "-d"; //删除任务队列中的一个任务
static const char* COMMAND_TASK_PRIOR = "-p"; //修改任务队列中的优先级
static const char* COMMAND_FRAME = "frame"; //任务框架相关
static const char* COMMAND_FRAME_INIT = "-i"; //初始化任务框架
static const char* COMMAND_FRAME_LOAD = "-l"; //动态显示任务框架的负载（类似top命令）
static const char* COMMAND_EXIT = "exit"; //退出指令
static const char* COMMAND_EXIT_NORMAL = "-n"; //停止接受新任务，在所有任务处理完毕后结束任务调度程序
static const char* COMMAND_EXIT_IMMEDIATE = "-i"; //立刻结束任务调度程序

int get_input(char buf[BUF_SIZE]);//获取输入的字符串存入buf
void parse(char* buf);
int do_cmd(int argc, char* argv[]);
void call_man();//输入首位为指令类型 次位为参数 再次为pid 再次为优先级
void timer();
int send(int pid, int sig, int prior);
#endif
