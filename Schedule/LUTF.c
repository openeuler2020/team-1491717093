/**************************************************************************
*	赛题4：LUTF - Linux Userspace Task Framework
*	团队：国五大人  1491717093
*
*	文件名：LUTF.c
*	简介：该文件为任务调度框架的主程序
* 
**************************************************************************/

struct LUTF_task_struct* curr_task;//当前任务指针，指向当前执行任务的LUTF_task_struct结构体
unsigned int task_queue_valid = 0x00000000;//表示32个任务队列
struct LUTF_task_queue* global_task_queue;//指向任务队列的全局指针
int handling_task;//表示是否正在处理任务函数中
unsigned long global_task_pid = 0;//全局任务pid
long int global_clock = 0;//全局时钟，相当于jiffies

#include "LUTF.h"

jmp_buf main_env;//函数间跳转时保存上下文的环境变量

int init_task_queue(struct LUTF_task_queue* task_queue)//任务队列初始化
{
	task_queue->task_num=0;
	task_queue->task_struct_start = NULL;

	return 0;
}

int start_task(struct LUTF_task_struct* task_struct, int i)
{
	//printf("*****\n");
	if (task_struct->if_jump == 1)//如果该任务有上下文保存，则跳转到保存的上下文继续执行
	{
		task_struct->if_jump = 0;
		handling_task = 1;//开始任务处理
		longjmp(task_struct->jump_addr, 1);
	}
	else//如果该任务没有上下文保存，则从头开始执行该任务
	{
		handling_task = 1;//开始任务处理
		switch (task_struct->type)
		{
			case 1: task_type_1(1, 2); break;
			case 2: task_type_2(1, 2); break;
			case 3: task_type_3(1, 2); break;
			case 4: task_type_4(1, 2); break;
			case 5: task_type_5(1.0, 2.0); break;
			case 6: task_type_6(1.0, 2.0); break;
			case 7: task_type_7(1.0, 2.0); break;
			case 8: task_type_8(1.0, 2.0); break;
			case 9: task_type_9(2.0); break;
			case 10: task_type_10(2.0); break;
			case 11: task_type_11(2.0); break;
			case 12: task_type_12(2.0); break;
			case 13: task_type_13(1.0, 2.0); break;
			case 14: task_type_14(2.0); break;
			case 15: task_type_15(2.0); break;
			case 16: task_type_16(2.0); break;
			case 17: task_type_17(2.0); break;
			case 18: task_type_18(2.0); break;
			case 19: task_type_19(2.0); break;
			case 20: task_type_20(2.0); break;
			default:;
		}
	}

	//从任务队列中移除该任务
	global_task_queue[task_struct->priority].task_num -= 1;
	if (global_task_queue[task_struct->priority].task_num == 0)//该优先级队列中已没有任务
		global_task_queue[task_struct->priority].task_struct_start = NULL;
	else//该优先级队列中尚有任务
	{
		global_task_queue[task_struct->priority].task_struct_start = task_struct->next_task;
		task_struct->next_task->pre_task = task_struct->pre_task;
		task_struct->pre_task->next_task = task_struct->next_task;
	}
	//printf("%d %d\n", task_struct, task_struct->next_task);
	
	if(task_struct->next_task==task_struct)
	{
		task_queue_valid-=1<<i;
	}

	free(task_struct);//释放任务结构体空间
	handling_task = 0;//结束任务处理
	return 0;
}

int main()//主函数
{
	int i = 0;//循环时使用的变量
	int state;//标志任务架构初始化成功与否
	struct LUTF_task_queue task_queue[32];//32个任务队列分别代表32个优先级
	global_task_queue = task_queue;

	//设置信号处理函数
	signal(SIG_TIMER, signal_clock_interrupt);
	signal(SIG_TASK_CREATE, signal_task_create);
	signal(SIG_EXIT_NORMAL, signal_exit_normal);

	printf("LUTF\n");


lutf_start://开始初始化任务框架
	//任务队列初始化
	task_queue_valid = 0x00000000;
	for (i = 0; i < 32; i++)
	{
		state = init_task_queue(&(task_queue[i]));
		
		
		if (state != 0)
		{
			printf("Fail to initialize task queue\n");
			exit(i+1);
		}
	}

	
	
	
task_handle_start://开始处理任务队列
	setjmp(main_env);//*******时钟中断信号处理函数结束后跳转到这里执行
	if(task_queue_valid==0x00000000)
		;//不作任何处理，进入休眠
	else//从高优先级队列开始选择任务并执行
	{
		for (i = 31; i >= 0; i--)
		{
			if (task_queue_valid >> i >= 1)
			{
				start_task(task_queue[31 - i].task_struct_start, i);
			}
		}
	}
	//如果任务队列中没有任务，则休眠100秒，如果休眠中有信号量传入，
	//则立刻开始执行信号量处理函数，之后重新开始处理任务队列（如果程序没有结束）
	sleep(100);
	goto task_handle_start;

lutf_end://任务调度框架正常退出
	printf("LUTF exits normally...\n");
	return 0;
}
