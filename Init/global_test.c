/**************************************************************************
*	赛题4：LUTF - Linux Userspace Task Framework
*	团队：国五大人  1491717093
*
*	文件名：global_init.c
*	简介：该文件为初始化程序的主程序
*
**************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main()
{
    //char dynamicpath[100] = "mv ./Schedule/lib/libs/libdynamic.so /usr/lib/";
    char LUTF_string[100] = "gnome-terminal -t \"LUTF\" -x bash -c \"./LUTF;exec bash;\"";
    char contrl_string1[100] = "gnome-terminal -t \"control\" -x bash -c \"./control test ";
    char contrl_string2[100] = " ;exec bash;\"";
    
    //system(dynamicpath);
    system(LUTF_string);//开启任务调度程序

    FILE* fp = popen("ps -e | grep \'LUTF\' | awk \'{print $1}\'", "r");//打开管道，执行shell 命令搜索LUTF进程对应的pid
    char buffer[10] = { 0 };
    fgets(buffer, 10, fp); //读取执行结果并储存
    
    pclose(fp);
    
    strcat(contrl_string1, buffer);
    strcat(contrl_string1, contrl_string2);
    
    system(contrl_string1);//开启控制程序，并传递任务调度程序的pid

    return 0;//初始化进程结束
}
