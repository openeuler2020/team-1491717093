/**************************************************************************
*	赛题4：LUTF - Linux Userspace Task Framework
*	团队：国五大人  1491717093
*
*	简介：该文件为控制程序的主体部分
*
**************************************************************************/

#include"control.h"

int get_input(char buf[BUF_SIZE]) //获取输入的字符串存入buf
{
    //int ret;
    memset(buf, 0, BUF_SIZE);
    fgets(buf, BUF_SIZE, stdin); //安全地读取输入字符串
    /*while (ret == 0) {
        while (getchar() != '\n');
        printf("Please input command.\n");
        printf("cmd:");
    }*/
    
    return 0;
}
void parse(char* buf)
{
    char* argv[BUF_SIZE];
    int argc = 0;
    int i = 0;
    int status = 0;
    for (i = 0; buf[i] != 0; i++) {
        if (status == 0 && !isspace(buf[i])) { //判断是否为空
            argv[argc++] = buf + i; //buf里的字符串导入argv
            status = 1;
        }
        else if (status = 1 && isspace(buf[i])) {
            status = 0;
            buf[i] = '\0'; //终止字符
        }
    }

    argv[argc] = NULL;
    do_cmd(argc, argv); //执行指令
}
int do_cmd(int argc, char* argv[])
{
    int sig = 0;
    int pid, prior;
    
    if (strcmp(argv[0], COMMAND_MAN) == 0) {
        call_man();
    }
    
    else if (strcmp(argv[0], COMMAND_TASK) == 0) {
        if (strcmp(argv[2], COMMAND_TASK_CREATE) == 0) {
            sig = SIG_TASK_CREATE;
            printf("create task\n");
        }
        /*else if (strcmp(argv[2], COMMAND_TASK_DELETE) == 0) {
            sig = SIG_TASK_DELETE;
            printf("delete task\n");
        }
        else if (strcmp(argv[2], COMMAND_TASK_PRIOR) == 0) {
            sig = SIG_TASK_PRIOR;
            printf("set prior task\n");
        }*/
        else {
            sig = SIG_ERROR;
            printf("Please input the right command.\n");
        }
    }
    /*else if (strcmp(argv[0], COMMAND_FRAME) == 0) {
        if (strcmp(argv[2], COMMAND_FRAME_INIT) == 0) {
            sig = SIG_FRAME_INIT;
            printf("init frame\n");
        }
        else if (strcmp(argv[2], COMMAND_FRAME_LOAD) == 0) {
            sig = SIG_FRAME_LOAD;
            printf("load frame\n");
        }
        else {
            sig = SIG_ERROR;
            printf("Please input the right command.\n");
        }
    }*/
    else if (strcmp(argv[0], COMMAND_EXIT) == 0) {
        //if (strcmp(argv[2], COMMAND_EXIT_NORMAL) == 0) {
            sig = SIG_EXIT_NORMAL;
            printf("exit normally\n");
        /*}
        else if (strcmp(argv[2], COMMAND_EXIT_IMMEDIATE) == 0) {
            sig = SIG_EXIT_IMMEDIATE;
            printf("exit immediately\n");
        }
        else {
            sig = SIG_ERROR;
            printf("Please input the right command.\n");
        }*/
    }
    else {
        sig = SIG_ERROR;
        printf("Please input the right command.\n");
    }
    
    /*if(argv[3]!=NULL)
        pid = atoi(argv[3]); //输入格式中pid需占第三位
    if(argv[4]!=NULL)
        prior = atoi(argv[4]); //输入格式中优先级prior需占第四位
    */
    send(pid, sig, 1);
    return 0;
}
void call_man() //输入首位为指令类型 次位为参数 再次为pid 再次为优先级
{
    printf("You must input commands as follows:\n");
    printf("type parameter pid priority\n");
    printf("use space to divide\n");
    printf("The usable types of commands and parameters are as follows:\n");
    printf("man: show usable commands\n");
    printf("task: add -c to create a task.\n");
    //printf("frame: add -i to init, add -l to load.\n");
    printf("exit: exit immediately.\n");
    //printf("pid should be an integer and priority should be between 0 and 100\n");
}

void timer()
{
    int sig_tv;
    int pid = 0; 
    int prior = 0; 
    while (TRUE) {
        sleep(1 / Hz);
        sig_tv = SIG_TIMER;
        send(pid, sig_tv, prior);
    }
}
int send(int pid, int sig, int prior)
{
    int obj_pid=global_pid;//任务调度程序pid
    if (sig < 0 || prior < 0 || prior > 31) {
        printf("error.\n");
        return 0;
    }
    else
    {
        kill(obj_pid, sig);
    }
    
    if(sig==SIG_EXIT_NORMAL)
    {
    	exit(0);
    }
    
    return 0;
}
int main(int argc, char* argv[])
{
    char buf[BUF_SIZE];
    //从初始化程序中得到任务调度程序的pid
    if (argc == 2)
    {
        global_pid = atoi(argv[1]);
        
    	int fpid = fork();//创建一个子进程运行定时器

    	if (fpid < 0)
        	printf("error in fork!");
    	else if (fpid == 0) {//子进程运行定时器，以系统中断频率发送时钟中断信号
        	timer();
    	}
    	else {
    	    while (TRUE) {//父进程继续读取输入，并发送相应信号
    	        printf("cmd:");
    	        get_input(buf);
    	        parse(buf);
    	    }
    	}
    }
    else if (argc == 3)
    {
    	int i=0;
    	global_pid = atoi(argv[2]);
    	
    	int fpid = fork();//创建一个子进程运行定时器

    	if (fpid < 0)
        	printf("error in fork!");
    	else if (fpid == 0) {//子进程运行定时器，以系统中断频率发送时钟中断信号
        	timer();
    	}
    	else {
    	    	for (i=0; i<1000000; i++) {//父进程继续读取输入，并发送相应信号
    	        	send(global_pid, SIG_TASK_CREATE, 1);
    	        	printf("%d ", i);
    	        	usleep(50);
    	    	}
    	    send(global_pid, SIG_EXIT_NORMAL, 1);
    	}
    }	
    else
    {
        printf("Please input a pid!\n");
        printf("Example: ./control 1000\n");
        return 0;
    }
    

    return 0;
}
