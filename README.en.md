# 4-国五大人

#### Description
TOPIC_ID:4, TEAM_ID:1491717093, TEAM_NAME:国五大人.

#### Software Architecture
Software architecture description

The task scheduling framework is divided into three modules: initialization program, control program and task scheduler.
The initialization program is responsible for starting the control program and task scheduler, and passing the necessary parameters (process PID, etc.) during the starting process, and then automatically ends the running.
The control program is responsible for receiving the command input and sending the corresponding signal to the task scheduler after processing. The control program will also send the clock signal to the task scheduler according to the system clock frequency.
In test mode, the test program is used instead of the initialization program.

#### Installation

1. In the Ubuntu operating system, download the source code of the project to any directory (it is recommended to use the system of Ubuntu 20.04).
2. Enter the "schedule / lib" directory, execute the "make" command with root authority to compile the dynamic library file (root authority is required to move the compiled dynamic library file to the directory of / usr / LIB).
3. Return to the main directory of the project, execute the "make" command with ordinary user rights to compile, and get init, test, control, lutf four executable files.
4. If you want to update the dynamic library file after modifying the dynamic library code, just re execute step 2.

#### Instructions

Normal use:

1. After compiling, enter the command of "." / init "in the main directory of the project to run the task scheduling framework.
2. After the execution of task scheduling framework, two new bash will be opened to execute control program and task scheduler respectively.
3. Users can input commands in bash of running control program (only support "man" to display help manual, "task - C" to create new task command, "exit" to exit task scheduling framework command), and view task running results in bash of task scheduler.



Test mode

1. After compiling, enter the command "test" in the main directory of the project to enter the test mode.
2. In the test mode, the control program will automatically send 1000000 task related commands to the task scheduler.(critical bug fixes)
3. Users can view the running results of tasks in bash of task scheduler.

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
